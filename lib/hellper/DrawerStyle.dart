import 'package:flutter/material.dart';

class DrawerStyle extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, size.height);
    path.quadraticBezierTo(size.height/4, size.height, size.width/2, size.height-40);
    path.quadraticBezierTo(size.width-100, size.height-100, size.width, size.height);
    path.quadraticBezierTo(size.width-100, size.height/2, size.width, 0);
    path.quadraticBezierTo(size.width/2, 40, 0, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return null;
  }

}
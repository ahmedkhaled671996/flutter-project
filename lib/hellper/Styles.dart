import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class Styles {

  TextStyle get textStyle => TextStyle(
    color: Colors.black,
    fontSize: 20.0,
    fontStyle: FontStyle.italic
  );

}
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:untitled/gui/textToSpeech/TextToS.dart';

import 'DrawerStyle.dart';
import 'Strings.dart';
import 'Styles.dart';



Widget StylishDrawer(BuildContext context){

  return ClipPath(
    clipper: DrawerStyle(),
    child: Container(
      width: 300,
      height: double.infinity,
      child: Stack(
        children: <Widget>[
          BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5.0,sigmaY: 5.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.black12.withOpacity(.9)
              ),
            ),
          ),
          Container(
            child: ListView(
//              padding: const EdgeInsets.only(top: 30,left: 30,right: 40,bottom: 70),
              children: <Widget>[

                Card(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: 0,height: 10,
                      ),
                      SizedBox(
                        width: 100,
                        height: 100,
                        child: CircleAvatar(
                          child: Icon(
                              Icons.threed_rotation
                          ),
//                          backgroundImage: NetworkImage("https://pngimage.net/wp*content/uploads/2018/06/minato-namikaze-png-7.png"),
                        ),
                      ),
                      SizedBox(
                        width: 0,height: 10,
                      ),
                      Text(
                        "ahmed",
                        style: Styles().textStyle,
                      ),
                      SizedBox(
                        width: 0,height: 10,
                      ),
                    ],
                  ),
                ),
                ListTile(

                  title: Text(
                    Strings().textToS,
                    style: Styles().textStyle,
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TextToS()));
                  },
                ),
                ListTile(
                  title: Text(
                    Strings().sToText,
                    style: Styles().textStyle,
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TextToS()));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
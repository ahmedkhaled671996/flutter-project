import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class Strings {

  String get textToS => "Text To Speech";

  String get sToText => "Speech To Text";

  String get start => "Start";

  String get areUSure => "Are You Sure!!";

  String get doUWannaExitApp => "do you Wanna Exit an App";

  String get yes => "Yes";

  String get no => "No";

  String get Welcome => "Welcome";

  String get readFromServer => "readFromServer";

}
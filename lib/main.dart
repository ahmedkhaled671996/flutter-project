import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:untitled/gui/sToText/SToText.dart';
import 'package:untitled/gui/textToSpeech/TextToS.dart';
import 'package:untitled/hellper/Strings.dart';

import 'gui/readFromServer/ReadFromServer.dart';
import 'hellper/DrawerStyle.dart';
import 'hellper/Styles.dart';
import 'hellper/StylishDrawer.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            Strings().Welcome,
            style: Styles().textStyle,
          ),
          backgroundColor: Colors.black26,
        ),
        body: Container(
          child: ListView(
//              padding: const EdgeInsets.only(top: 30,left: 30,right: 40,bottom: 70),
            children: <Widget>[
              Card(
                margin: EdgeInsets.all(5),
                child: ListTile(
                  title: Text(
                    Strings().textToS,
                    style: Styles().textStyle,
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TextToS()));
                  },
                ),
              ),

              /**Card(
                margin: EdgeInsets.all(5),
                child: ListTile(
                  title: Text(
                    Strings().sToText,
                    style: Styles().textStyle,
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SToText()));
                  },
                ),
              ),**/

              Card(
                margin: EdgeInsets.all(5),
                child: ListTile(
                  title: Text(
                    Strings().readFromServer,
                    style: Styles().textStyle,
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ReadFromServer()));
                  },
                ),
              ),
            ],
          )
        ),
      ),
    );
  }

}


import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:toast/toast.dart';
import 'package:untitled/gui/readFromServer/ApiClint.dart';
import 'package:untitled/gui/readFromServer/models/User.dart';
import 'package:untitled/hellper/Strings.dart';
import 'package:untitled/hellper/Styles.dart';

class ReadFromServer extends StatefulWidget {
  _ReadFromServerState createState() => _ReadFromServerState();

}

class _ReadFromServerState extends State<ReadFromServer>  {

  List data = [];
  String ahmed = "";
  bool addUserBool;
  void getMyData() async{
    data = await(ApiClint().getData());
    print(data);
    setState(() {
        ahmed = data.toString();
    });
  }

  void addNewUser(User user) async{
    addUserBool = await(ApiClint().addUser(user));
    if(addUserBool){
      Toast.show("Done...", context);
    }else{
      Toast.show("Error...", context);
    }
  }

  @override
  Widget build(BuildContext context)  {
    getMyData();
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text(
          Strings().readFromServer,
          style: Styles().textStyle,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.person_add,
        ),
        onPressed: () {
//          addNewUser(new User("nkjbvhbkjd", "wigdi", "wigdi@email.com"));
          showDialog(context: context ,builder: (BuildContext context){
            TextEditingController name , email;
            User user ;
            return AlertDialog(
                title: Text(
                    "add User"
                ),
                content: Column(
                    children: <Widget>[
                      TextField(
                        controller: name,
                        style: Styles().textStyle,
                        decoration: InputDecoration(hintText: "name"),
                      ),
                      TextField(
                        controller: email,
                        style: Styles().textStyle,
                        decoration: InputDecoration(hintText: "email"),
                      )
                    ],
                  ),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      "add"
                    ),
                    onPressed: () {
                        user = new User(
                            DateTime.now().millisecondsSinceEpoch.toString(),
                            name.text,
                            email.text
                        );
                        Toast.show(user.name, context);
                    },
                  ),
                ],
              );
            }
          );
        }
      ),
      body: Container(
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (BuildContext context,int position){
//              if(position.isEven) return new Divider();
                return new Slidable(
                  actionPane: SlidableBehindActionPane(),
                  actionExtentRatio: 0.25,
                  child: new Container(
                    color: Colors.white,
                    child: new ListTile(
                      onTap: () {
                        print("otTap");
                        Toast.show("otTap", context);
                      },
                      onLongPress: () {
                        print("onLongPress");
                        Toast.show("onLongPress", context);
                      },
                      leading: new CircleAvatar(
                        backgroundColor: Colors.indigoAccent,
                        child: new Text('I $position'),
                        foregroundColor: Colors.white,
                      ),
                      title: new Text("${data[position]['name']}"),
                      subtitle: new Text('${data[position]['email']}'),
                    ),
                  ),
                  actions: <Widget>[
                    new IconSlideAction(
                      caption: 'Archive',
                      color: Colors.blue,
                      icon: Icons.archive,
                      onTap: () {
                        print("Archive");
                        Toast.show("Archive", context);
                      }
                    ),
                    new IconSlideAction(
                      caption: 'Share',
                      color: Colors.indigo,
                      icon: Icons.share,
                      onTap: () {
                        print("Share");
                        Toast.show("Share", context);
                      }
                    ),
                  ],
                  secondaryActions: <Widget>[
                    new IconSlideAction(
                      caption: 'More',
                      color: Colors.black45,
                      icon: Icons.more_horiz,
                      onTap: () {
                        print("More");
                        Toast.show("More", context);
                      }
                    ),
                    new IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        print("Delete");
                        Toast.show("Delete", context);
                      }
                    ),
                  ],
                );
                  /**new ListTile(


                    title: Text(
                      "${data[position]['name']}",
                      style: Styles().textStyle,
                    ),
                  subtitle: Text(
                    "${data[position]['email']}",
                    style: Styles().textStyle,
                  ),
                  leading: CircleAvatar(
                    child: Icon(
                        Icons.threed_rotation
                    ),
                    backgroundColor: Colors.amber,
                    foregroundColor: Colors.white70,
                  ),
                );**/
            }
        ),
      ),
    );
  }
}


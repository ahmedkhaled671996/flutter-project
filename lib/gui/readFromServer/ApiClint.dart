import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:untitled/gui/readFromServer/models/User.dart';

final String baseUrl = 'https://zsnuwman.000webhostapp.com/WCIDAPP/';

class ApiClint{

  Future<List> getData() async{
    http.Response response = await http.get(baseUrl+"raedContacts.php");
    return json.decode(response.body);
  }

  Future<bool> addUser(User user) async{

    http.Response response = await http.post(baseUrl+"writeToDB.php",body: {
      "id":user.id,
      "name":user.name,
      "email":user.email
    });

    if(response.statusCode==200){
      return true;
    }else{
      return false;
    }

  }


}


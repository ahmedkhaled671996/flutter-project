import 'package:flutter/material.dart';
import 'package:untitled/hellper/Strings.dart';
import 'package:untitled/hellper/Styles.dart';
import 'package:flutter_tts/flutter_tts.dart';

class TextToS extends StatefulWidget{
  _TextToSState createState() => _TextToSState();
}

class _TextToSState extends State<TextToS>{

  final FlutterTts flutterTts = FlutterTts();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var textEditingController = TextEditingController();
    return  Scaffold(
//      drawer: StylishDrawer(context),
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text(
          Strings().textToS,
          style: Styles().textStyle,
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextField(
              controller: textEditingController,
              style: Styles().textStyle,
              onEditingComplete: () async {
                if(textEditingController.text.isNotEmpty){
                  await flutterTts.speak(textEditingController.text.toString());
                }
              },
            ),
            SizedBox(
              width: 0,
              height: 20,
            ),
            RaisedButton(
              child: Text(
                Strings().textToS,
                style: Styles().textStyle,
              ),
              onPressed: () async {
                if(textEditingController.text.isNotEmpty){
                  await flutterTts.speak(textEditingController.text.toString());
                }
              },
            )
          ],
        ),
      ),
    );
  }

}
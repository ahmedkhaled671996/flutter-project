import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:untitled/gui/textToSpeech/TextToS.dart';
import 'package:untitled/hellper/Strings.dart';
import 'package:untitled/hellper/Styles.dart';
import 'package:untitled/hellper/StylishDrawer.dart';

class SToText extends StatefulWidget{
  _SToTextState createState() => _SToTextState();
}

class _SToTextState extends State<SToText>{
  SpeechRecognition _speechRecognition ;
  bool _isAvailable = false;
  bool _isListenire = false;

  String resultText = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void initSpeechRecognizer(){
    _speechRecognition = SpeechRecognition();
    _speechRecognition.setAvailabilityHandler(
        (bool result) => setState(() => _isAvailable = result),
    );
    _speechRecognition.setRecognitionStartedHandler(
        () => setState(() => _isListenire = true),
    );
    _speechRecognition.setRecognitionResultHandler(
        (String speech) => setState(() => resultText = speech),
    );
    _speechRecognition.setRecognitionCompleteHandler(
        () => setState(() => _isListenire = false),
    );
    _speechRecognition.activate().then(
        (result) => setState(() => _isAvailable = result),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
//          drawer: StylishDrawer(context),
          appBar: AppBar(
            backgroundColor: Colors.amber,
            title: Text(
              Strings().sToText,
              style: Styles().textStyle,
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(20),
            child: Center(
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(
                        Icons.cancel
                    ),
                    mini: true,
                    backgroundColor: Colors.amber,
                    onPressed: () {
                      if(_isListenire){
                        _speechRecognition
                            .cancel()
                            .then((result)=>setState(()  {
                          _isListenire = result;
                          resultText = "";
                        }
                        ));
                      }
                    },
                  ),
                  FloatingActionButton(
                    child: Icon(
                        Icons.mic
                    ),
                    backgroundColor: Colors.black38,
                    onPressed: () {
                      if(_isAvailable&& !_isListenire){
                        _speechRecognition
                            .listen(locale: "en-US")
                            .then((result)=>print('$result'));
                      }
                    },
                  ),
                  FloatingActionButton(
                    child: Icon(
                        Icons.stop
                    ),
                    mini: true,
                    backgroundColor: Colors.pink,
                    onPressed: () {
                      if(_isListenire){
                        _speechRecognition
                            .stop()
                            .then((result)=>setState(() => _isListenire=result));
                      }
                    },
                  ),
                  Container(
                    child: Text(
                      resultText,
                      style: Styles().textStyle,
                    ),
                  ),
                ],
              ),
            ),
          ),
      );
  }

  Future<bool> _onWillPop(){
    return showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: Text(Strings().areUSure),
          content: Text(Strings().doUWannaExitApp),
          actions: <Widget>[
            FlatButton(
              child: Text(Strings().yes),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text(Strings().no),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
          ],
        )
    )??false;
  }

}